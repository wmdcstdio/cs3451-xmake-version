add_rules("mode.debug","mode.release")
add_requires("eigen >=3.4.0")
add_requires("freeglut")
if not is_plat("macosx") then
    add_requires("freeglut")
else 
    add_requires("glut")
end
add_requires("stb")
add_requires("tinyobjloader")
target("core")
    set_kind("static")
    set_languages("c++17")
    add_headerfiles("*.h")
    add_files("*.cpp")
    add_includedirs(".",{public=true})
    add_includedirs("./../ext/glm",{public=true})
    add_defines("GLM_ENABLE_EXPERIMENTAL",{public=true})

    add_packages("eigen",{public=true})
    --add_packages("my-glm",{public=true})
    if not is_plat("macosx") then
        add_packages("freeglut",{public=true})
    else 
        add_packages("glut",{public=true})
    end
    add_packages("stb",{public=true})
    add_packages("tinyobjloader",{public=true})
