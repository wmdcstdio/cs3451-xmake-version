# CS3451 Computer Graphics Starter Code

### Code Structure

The starter codebase is organized as `ext`, `src`, `assignments` and `tutorials`. We carry some external code (e.g., `glm`) in `ext`. We put the common headers that can be shared among different subprojects in `src` (e.g., the classes of mesh, grid, file IO, etc.). The folder `assignments` maintains all assignments. An assignment is dependent on `src` and `ext`, but is independent from other assignments.

Usually, you are asked to write code in one or multiple files in an assignment folder. You don’t need to change files in ext or src.

### Build and Compile

We use [xmake](https://xmake.io/#/) for compilation.

The executables are generated under a path in the form of `cs3451-computer-graphics-starter-code/build/[your-os]/x64/release`. For example, `cs3451-computer-graphics-starter-code/build/windows/x64/release`

### Video Tutorial:
Watch the video tutorials made by our awesome TAs Kevin and Vibha:
- **Windows**: https://youtu.be/f6KYyLUswcg
- **OSX**: https://youtu.be/44jzYA2upHU

## 1. Compile and run the first assignment:

### Step 1: Clone the source code from GitLab and enter the codebase folder

    git clone git@gitlab.com:wmdcstdio/cs3451-xmake-version.git
    cd cs3451-xmake-version

### Step 2: Install xmake

Follow the instructions to install `xmake`:

https://xmake.io/#/getting_started

For example, if you're using Windows, it's recommended to open `Powershell` by search "Powershell" in start menu and run it (*without* Admin), then just run the following command:

    Invoke-Expression (Invoke-Webrequest 'https://xmake.io/psget.text' -UseBasicParsing).Content

### Step 3: Compile and run the code

To test if everything works correctly you can run:

    xmake build a1
    xmake run a1

This will:

- Compile the code for the assignment 1 and any dependencies
- Popup a window and show an OpenGL window. You should see this image if everything works properly: ![a1image](misc/a1.JPG)  

### Step 4: Start implementing!

Use your editor of choice to edit the assignment files in `assignments`.

Recommended: open the folder `cs3451-computer-graphics-starter-code` in Visual Studio Code, and start editing!