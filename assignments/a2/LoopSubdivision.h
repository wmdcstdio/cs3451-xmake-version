#ifndef __LoopSubdivision_h__
#define __LoopSubdivision_h__
#include <unordered_map>
#include <vector>
#include "Mesh.h"

inline void LoopSubdivision(TriangleMesh<3>& mesh)
{
	std::vector<Vector3>& old_vtx=mesh.Vertices();
	std::vector<Vector3i>& old_tri=mesh.Elements();
	std::vector<Vector3> new_vtx;		////vertex array for the new mesh
	std::vector<Vector3i> new_tri;		////element array for the new mesh
	
	new_vtx=old_vtx;	////copy all the old vertices to the new_vtx array

	////step 1: add mid-point vertices and triangles
	////for each old triangle, 
	////add three new vertices (in the middle of each edge) to new_vtx 
	////add four new triangles to new_tri
	std::unordered_map<Vector2i,int> edge_vtx_map;
	
	for(int i=0;i<old_tri.size();i++){
		const Vector3i& tri=old_tri[i];
		Vector3i tri_mid;
		for(int j=0;j<3;j++){
			Vector2i edge(tri[j],tri[(j+1)%3]);
			if(edge[0]>edge[1]){int tmp=edge[0];edge[0]=edge[1];edge[1]=tmp;}
			int mid_idx=0;
			if(edge_vtx_map.find(edge)==edge_vtx_map.end()){
				Vector3 vtx=.5*(old_vtx[edge[0]]+old_vtx[edge[1]]);
				new_vtx.push_back(vtx);
				mid_idx=(int)(new_vtx.size()-1);
				edge_vtx_map[edge]=mid_idx;
			}
			else mid_idx=edge_vtx_map[edge];
			tri_mid[j]=mid_idx;
		}
		new_tri.push_back(Vector3i(tri[0],tri_mid[0],tri_mid[2]));
		new_tri.push_back(Vector3i(tri[1],tri_mid[1],tri_mid[0]));
		new_tri.push_back(Vector3i(tri[2],tri_mid[2],tri_mid[1]));
		new_tri.push_back(Vector3i(tri_mid[0],tri_mid[1],tri_mid[2]));
	}

	////step 2: update the position for each new mid-point vertex: 
	////for each mid-point vertex, find its two end-point vertices A and B, 
	////and find the two opposite-side vertices on the two incident triangles C and D,
	////then update the new position as .375*(A+B)+.125*(C+D)

	////update position
	std::vector<Vector3> smt_vtx=new_vtx;
	std::unordered_map<Vector2i,Vector2i> edge_tri_map;
	std::unordered_map<int,std::vector<int> > vtx_vtx_map;

	for(int i=0;i<old_tri.size();i++){
		const Vector3i& tri=old_tri[i];
		for(int j=0;j<3;j++){
			Vector2i edge(tri[j],tri[(j+1)%3]);
			if(edge[0]>edge[1]){int tmp=edge[0];edge[0]=edge[1];edge[1]=tmp;}
			if(edge_tri_map.find(edge)==edge_tri_map.end())
				edge_tri_map[edge]=Vector2i(i,-1);
			else{edge_tri_map[edge][1]=i;}
		}
	}

	for(auto& iter:edge_vtx_map){
		Vector2i e=iter.first;
		int v=iter.second;
		const Vector2i& edge_inc_tris=edge_tri_map[e];
		Vector2i op_vtx;
		bool use_smt=true;
		for(int i=0;i<2;i++){
			if(edge_inc_tris[i]==-1){
				use_smt=false;
				continue;}	////boundary
			const Vector3i& tri=old_tri[edge_inc_tris[i]];
			for(int j=0;j<3;j++){
				if(tri[j]!=e[0]&&tri[j]!=e[1]){
					op_vtx[i]=tri[j];
					break;
				}
			}
		}
		if(use_smt)smt_vtx[v]=.375*old_vtx[e[0]]+.375*old_vtx[e[1]]+.125*old_vtx[op_vtx[0]]+.125*old_vtx[op_vtx[1]];
	}

	// ////step 3: update vertices of each old vertex
	// ////for each old vertex, find its incident old vertices, and update its position according its incident vertices
	// for(auto& iter:edge_vtx_map){
	// 	Vector2i e=iter.first;
	// 	for(int j=0;j<2;j++){
	// 		int v0=e[j];int v1=e[(j+1)%2];
	// 		if(vtx_vtx_map.find(v0)==vtx_vtx_map.end()){
	// 			vtx_vtx_map[v0]=std::vector<int>();}
	// 		vtx_vtx_map[v0].push_back(v1);
	// 	}
	// }

	// for(auto& iter:vtx_vtx_map){
	// 	int v=iter.first;
	// 	const std::vector<int>& nbs=iter.second;
	// 	real beta=.375;
	// 	if(nbs.size()>3)beta=.375/(nbs.size());
	// 	real alpha=1.-(real)nbs.size()*beta;
	// 	Vector3 pos=alpha*old_vtx[v];
	// 	for(int i=0;i<(int)nbs.size();i++){
	// 		pos+=old_vtx[nbs[i]]*beta;
	// 	}
	// 	smt_vtx[v]=pos;
	// 	std::cout<<v<<": "<<smt_vtx[v].transpose()<<", "<<std::endl;
	// }

	 new_vtx=smt_vtx;

	////update subdivided vertices and triangles onto the input mesh
	old_vtx=new_vtx;
	old_tri=new_tri;
}

#endif